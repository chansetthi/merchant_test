package com.test.backend.test_backend.implement;

import com.test.backend.test_backend.dto.*;
import com.test.backend.test_backend.entity.Category;
import com.test.backend.test_backend.entity.CategoryRefProduct;
import com.test.backend.test_backend.entity.Merchant;
import com.test.backend.test_backend.entity.Product;
import com.test.backend.test_backend.enumuration.CommonResStatus;
import com.test.backend.test_backend.repository.CategoryRefProductRepository;
import com.test.backend.test_backend.repository.CategoryRepository;
import com.test.backend.test_backend.repository.ProductRepository;
import com.test.backend.test_backend.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

@Service
public class CategoryImpl implements CategoryService {
    @Autowired
    CategoryRepository categoryRepo;
    @Autowired
    ProductRepository productRepo;
    @Autowired
    CategoryRefProductRepository categoryRefProductRepo;

    @Override
    public ResponseDTO createCategory(RequestCreateCategoryDTO req) {
        if (!validateCreate(req)) {
            return setResponse(CommonResStatus.INVALID_INPUT_PARAMETER, null);
        }
        Category category = new Category();
        if (categoryRepo.existsByCategoryName(req.getNameCategory())) {
            return setResponse(CommonResStatus.DUPLICATE_CATEGORY, null);
        }
        UUID uuid = UUID.randomUUID();
        do {
            uuid = UUID.randomUUID();
        } while (categoryRepo.existsByUuidCategory(uuid.toString()));
        category.setUuidCategory(uuid.toString());
        category.setVersion(1L);
        category.setCreatedDate(new Date());
        category.setCategoryName(req.getNameCategory());
        category.setDescription(req.getDescription());
        categoryRepo.save(category);
        return setResponse(CommonResStatus.SUCCESS, null);
    }

    @Override
    public ResponseDTO list(RequestListCategory req) {
        Pageable pageable = pageable(req.getPageSize(), req.getPageNumber());
        Page<Category> categories = categoryRepo.findAllByCategoryNameIgnoreCaseContaining(req.getText(), pageable);
        PageDTO pageDTO = new PageDTO();
        pageDTO.setContent(categories.getContent());
        pageDTO.setPageNumber(categories.getNumber());
        pageDTO.setTotalPages(categories.getTotalPages());
        pageDTO.setTotalElements(categories.getTotalElements());
        return setResponse(CommonResStatus.SUCCESS, pageDTO);
    }


    @Override
    public ResponseDTO categoryDetail(RequestAddProductInCategoryDTO req) {
        Category category = categoryRepo.findByUuidCategory(req.getUuidCategory());
        if (category == null){
            return setResponse(CommonResStatus.EXISTS_CATEGORY, null);
        }
        category.setId(null);
        return setResponse(CommonResStatus.SUCCESS, category);
    }
    @Override
    public ResponseDTO addProduct(RequestAddProductInCategoryDTO req) {
        CategoryRefProduct categoryRefProduct = new CategoryRefProduct();
        Category category = categoryRepo.findByUuidCategory(req.getUuidCategory());
        Product product = productRepo.findByUuid(req.getUuidProduct());
        if (category == null) {
            return setResponse(CommonResStatus.EXISTS_CATEGORY, null);
        }
        if (product == null) {
            return setResponse(CommonResStatus.EXISTS_PRODUCT, null);
        }
        categoryRefProduct.setVersion(1L);
        categoryRefProduct.setCreatedDate(new Date());
        categoryRefProduct.setCategory(category);
        categoryRefProduct.setProduct(product);
        categoryRefProductRepo.save(categoryRefProduct);
        return setResponse(CommonResStatus.SUCCESS, null);
    }

    private ResponseDTO setResponse(CommonResStatus status, Object data) {
        ResponseDTO res = new ResponseDTO();
        res.setCode(status.getCode());
        res.setMessage(status.getMessage());
        res.setData(data);
        return res;
    }

    public Pageable pageable(String pageSize, String pageNo) {
        int pageSizeInt = Integer.parseInt(pageSize);
        int pageNoInt = Integer.parseInt(pageNo);
        Pageable pageable = PageRequest.of(pageNoInt - 1, pageSizeInt);
        return pageable;
    }

    private Boolean validateCreate(RequestCreateCategoryDTO req) {
        if (isNullOrEmpty(req.getNameCategory())) {
            return false;
        }
        if (isNullOrEmpty(req.getDescription())) {
            return false;
        }
        return true;
    }

    public static boolean isNullOrEmpty(String text) {
        return text == null || text.length() == 0; // string.isEmpty() in Java 6
    }

}
