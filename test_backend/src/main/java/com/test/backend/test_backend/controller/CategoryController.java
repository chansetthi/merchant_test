package com.test.backend.test_backend.controller;

import com.test.backend.test_backend.dto.RequestAddProductInCategoryDTO;
import com.test.backend.test_backend.dto.RequestCreateCategoryDTO;
import com.test.backend.test_backend.dto.RequestListCategory;
import com.test.backend.test_backend.dto.ResponseDTO;
import com.test.backend.test_backend.dto.merchant.RequestCreateMerchantDTO;
import com.test.backend.test_backend.service.CategoryService;
import com.test.backend.test_backend.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/category")
public class CategoryController extends CommonUtil {

    @Autowired
    CategoryService categoryService;

    @RequestMapping("/create")
    @ResponseBody
    public String createCategory(@RequestBody RequestCreateCategoryDTO req){
        ResponseDTO res = categoryService.createCategory(req);
        return parseObjectToJSON(res);
    }

    @RequestMapping("/list")
    @ResponseBody
    public String listCategory(@RequestBody RequestListCategory req){
        ResponseDTO res = categoryService.list(req);
        return parseObjectToJSON(res);
    }

    @RequestMapping("/detail")
    @ResponseBody
    public String detailCategory(@RequestBody RequestAddProductInCategoryDTO req){
        ResponseDTO res = categoryService.categoryDetail(req);
        return parseObjectToJSON(res);
    }

    @RequestMapping("/addProduct")
    @ResponseBody
    public String addProductInCategory(@RequestBody RequestAddProductInCategoryDTO req){
        ResponseDTO res = categoryService.addProduct(req);
        return parseObjectToJSON(res);
    }
}
