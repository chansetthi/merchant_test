package com.test.backend.test_backend.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
public class Main {

    Logger logger = LogManager.getLogger(Main.class);

    @RequestMapping(value = "/")
    public String Home() {
        logger.info("Merchant is Online (>.<)");
        return "Merchant is Online (>.<)";
    }
}
