package com.test.backend.test_backend.service;

import com.test.backend.test_backend.dto.RequestAddProductInCategoryDTO;
import com.test.backend.test_backend.dto.RequestCreateCategoryDTO;
import com.test.backend.test_backend.dto.RequestListCategory;
import com.test.backend.test_backend.dto.ResponseDTO;

public interface CategoryService {
    ResponseDTO createCategory(RequestCreateCategoryDTO req);
    ResponseDTO list(RequestListCategory req);
    ResponseDTO categoryDetail(RequestAddProductInCategoryDTO req);
    ResponseDTO addProduct(RequestAddProductInCategoryDTO req);
}
