package com.test.backend.test_backend.repository;

import com.test.backend.test_backend.entity.Merchant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface MerchantRepository extends JpaRepository<Merchant, Long> {
    Merchant findByUuid(String uuid);
    Merchant findByMerchantName(String name);
    Page<Merchant> findAllByMerchantNameIgnoreCaseContaining(String name, Pageable pageable);
    boolean existsByMerchantName(String name);
    boolean existsByUuid(String uuid);
}
