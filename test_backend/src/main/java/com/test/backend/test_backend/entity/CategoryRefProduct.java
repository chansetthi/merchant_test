package com.test.backend.test_backend.entity;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "CATEGORY_REF_PRODUCT")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class CategoryRefProduct extends BaseEntity{
    @ManyToOne
    Category category = new Category();
    @ManyToOne
    Product product = new Product();

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
