package com.test.backend.test_backend.controller;

import com.test.backend.test_backend.dto.ResponseDTO;
import com.test.backend.test_backend.dto.product.RequestAddProductDTO;
import com.test.backend.test_backend.dto.product.RequestCreateProductDTO;
import com.test.backend.test_backend.dto.product.RequestListProduct;
import com.test.backend.test_backend.service.ProductService;
import com.test.backend.test_backend.util.CommonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/product")
public class ProducrController extends CommonUtil {

    @Autowired
    ProductService productService;

    @RequestMapping("/create")
    @ResponseBody
    public String createProduct(@RequestBody RequestCreateProductDTO req){
        ResponseDTO res = productService.createProduct(req);
        return parseObjectToJSON(res);
    }

    @RequestMapping("/edit")
    @ResponseBody
    public String editProduct(@RequestBody RequestCreateProductDTO req){
        ResponseDTO res = productService.editProduct(req);
        return parseObjectToJSON(res);
    }

    @RequestMapping("/delete")
    @ResponseBody
    public String deleteProduct(@RequestBody RequestCreateProductDTO req){
        ResponseDTO res = productService.deleteProduct(req);
        return parseObjectToJSON(res);
    }

    @RequestMapping("/detail")
    @ResponseBody
    public String detailProduct(@RequestBody RequestCreateProductDTO req){
        ResponseDTO res = productService.detailProduct(req);
        return parseObjectToJSON(res);
    }

    @RequestMapping("/list")
    @ResponseBody
    public String listProduct(@RequestBody RequestListProduct req){
        ResponseDTO res = productService.list(req);
        return parseObjectToJSON(res);
    }

    @RequestMapping("/list_in_merchant")
    @ResponseBody
    public String listInMerchant(@RequestBody RequestListProduct req){
        ResponseDTO res = productService.listInMerchant(req);
        return parseObjectToJSON(res);
    }

    @RequestMapping("/addProduct")
    @ResponseBody
    public String addProduct(@RequestBody RequestAddProductDTO req){
        ResponseDTO res = productService.addProduct(req);
        return parseObjectToJSON(res);
    }
}
