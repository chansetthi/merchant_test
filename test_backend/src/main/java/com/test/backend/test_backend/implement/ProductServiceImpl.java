package com.test.backend.test_backend.implement;

import com.test.backend.test_backend.dto.PageDTO;
import com.test.backend.test_backend.dto.ResponseDTO;
import com.test.backend.test_backend.dto.product.RequestAddProductDTO;
import com.test.backend.test_backend.dto.product.RequestCreateProductDTO;
import com.test.backend.test_backend.dto.product.RequestListProduct;
import com.test.backend.test_backend.dto.product.ResponseProductDetailDTO;
import com.test.backend.test_backend.entity.Merchant;
import com.test.backend.test_backend.entity.MerchantRefProduct;
import com.test.backend.test_backend.entity.Product;
import com.test.backend.test_backend.enumuration.CommonResStatus;
import com.test.backend.test_backend.repository.MerchantRefProductRepository;
import com.test.backend.test_backend.repository.MerchantRepository;
import com.test.backend.test_backend.repository.ProductRepository;
import com.test.backend.test_backend.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    ProductRepository productRepo;
    @Autowired
    MerchantRepository merchantRepo;
    @Autowired
    MerchantRefProductRepository merchantRefProductRepo;

    @Override
    public ResponseDTO createProduct(RequestCreateProductDTO req) {
        Product product = new Product();
        UUID uuid = UUID.randomUUID();
        if (!validateProduct(req)) {
            return setResponse(CommonResStatus.INVALID_INPUT_PARAMETER, null);
        }
        if (productRepo.existsByProductName(req.getProductName())) {
            return setResponse(CommonResStatus.DUPLICATE_PRODUCT_NAME, null);
        }
        product.setCreatedDate(new Date());
        product.setUuid(uuid.toString());
        product.setVersion(1L);
        product.setProductName(req.getProductName());
        product.setProductDescription(req.getProductDescription());
        product.setPrice(req.getPrice());
        product.setUnit(req.getUnit());
        productRepo.save(product);
        return setResponse(CommonResStatus.SUCCESS, setResponseProduct(product));
    }

    @Override
    public ResponseDTO editProduct(RequestCreateProductDTO req) {
        Product product = productRepo.findByUuid(req.getUuid());
        if (product == null) {
            return setResponse(CommonResStatus.EXISTS_PRODUCT, null);
        }
        if (productRepo.existsByProductName(req.getProductName())) {
            return setResponse(CommonResStatus.DUPLICATE_PRODUCT_NAME, null);
        }
        product.setLastUpdatedDate(new Date());
        product.setVersion(product.getVersion() + 1L);

        if (!isNullOrEmpty(req.getProductName())) {
            product.setProductName(req.getProductName());
        }
        if (!isNullOrEmpty(req.getProductDescription())) {
            product.setProductDescription(req.getProductDescription());
        }
        if (!isNullOrEmpty(req.getPrice())) {
            product.setPrice(req.getPrice());
        }
        if (!isNullOrEmpty(req.getUuid())) {
            product.setUnit(req.getUnit());
        }
        productRepo.save(product);
        return setResponse(CommonResStatus.SUCCESS, setResponseProduct(product));
    }

    @Override
    public ResponseDTO deleteProduct(RequestCreateProductDTO req) {
        Product product = productRepo.findByUuid(req.getUuid());
        if (product == null) {
            return setResponse(CommonResStatus.EXISTS_PRODUCT, null);
        }
        productRepo.delete(product);
        return setResponse(CommonResStatus.SUCCESS, null);
    }

    @Override
    public ResponseDTO detailProduct(RequestCreateProductDTO req) {
        Product product = productRepo.findByUuid(req.getUuid());
        if (product == null) {
            return setResponse(CommonResStatus.EXISTS_PRODUCT, null);
        }
        return setResponse(CommonResStatus.SUCCESS, setResponseProduct(product));
    }

    @Override
    public ResponseDTO list(RequestListProduct req) {
        List<ResponseProductDetailDTO> responseList = new ArrayList<>();
        Pageable pageable = pageable(req.getPageSize(), req.getPageNumber());
        Page<Product> productList = productRepo.findAllByProductNameIgnoreCaseContaining(req.getText(), pageable);
        for (Product product : productList) {
            responseList.add(setResponseProduct(product));
        }
        PageDTO pageDTO = new PageDTO();
        pageDTO.setContent(responseList);
        pageDTO.setPageNumber(productList.getNumber() + 1);
        pageDTO.setPageSize(productList.getSize());
        pageDTO.setTotalPages(productList.getTotalPages());
        pageDTO.setTotalElements(productList.getTotalElements());
        return setResponse(CommonResStatus.SUCCESS, pageDTO);
    }

    @Override
    public ResponseDTO listInMerchant(RequestListProduct req) {
        Pageable pageable = pageable(req.getPageSize(), req.getPageNumber());
        Merchant merchant = merchantRepo.findByMerchantName(req.getMerchantName());
        List<Product> productList = new ArrayList<>();
        if (merchant == null) {
            return setResponse(CommonResStatus.EXISTS_MERCHANT, null);
        }
        Page<MerchantRefProduct> merchantRefProducts = merchantRefProductRepo.findByMerchant(merchant, pageable);
        for (MerchantRefProduct merchantRefProduct : merchantRefProducts) {
            Optional<Product> product = productRepo.findById(merchantRefProduct.getProduct().getId());
            productList.add(product.get());
        }
        PageDTO pageDTO = new PageDTO();
        pageDTO.setContent(productList);
        pageDTO.setPageSize(merchantRefProducts.getSize());
        pageDTO.setPageNumber(merchantRefProducts.getNumber());
        pageDTO.setTotalPages(merchantRefProducts.getTotalPages());
        pageDTO.setTotalElements(merchantRefProducts.getTotalElements());
        return setResponse(CommonResStatus.SUCCESS, pageDTO);
    }

    @Override
    public ResponseDTO addProduct(RequestAddProductDTO req) {
        Merchant merchant = merchantRepo.findByUuid(req.getUuidMerchant());
        Product product = productRepo.findByUuid(req.getUuidProduct());
        if (merchant == null) {
            return setResponse(CommonResStatus.EXISTS_MERCHANT, null);
        }
        if (product == null) {
            return setResponse(CommonResStatus.EXISTS_PRODUCT, null);
        }
        if (merchantRefProductRepo.existsByMerchantAndProduct(merchant, product)) {
            return setResponse(CommonResStatus.DUPLICATE_PRODUCT_IN_MERCHANT, null);
        }
        MerchantRefProduct merchantRefProduct = new MerchantRefProduct();
        merchantRefProduct.setVersion(1L);
        merchantRefProduct.setCreatedDate(new Date());
        merchantRefProduct.setMerchant(merchant);
        merchantRefProduct.setProduct(product);
        merchantRefProductRepo.save(merchantRefProduct);
        return setResponse(CommonResStatus.SUCCESS, null);
    }

    private Pageable pageable(String pageSize, String pageNo) {
        int pageSizeInt = Integer.parseInt(pageSize);
        int pageNoInt = Integer.parseInt(pageNo);
        Pageable pageable = PageRequest.of(pageNoInt - 1, pageSizeInt);
        return pageable;
    }

    private ResponseProductDetailDTO setResponseProduct(Product product) {
        ResponseProductDetailDTO res = new ResponseProductDetailDTO();
        res.setRemark(product.getRemark());
        res.setVersion(product.getVersion());
        res.setCreatedBy(product.getCreatedBy());
        res.setCreatedDate(product.getCreatedDate());
        res.setLastUpdatedBy(product.getLastUpdatedBy());
        res.setLastUpdatedDate(product.getLastUpdatedDate());

        res.setUuid(product.getUuid());
        res.setProductName(product.getProductName());
        res.setProductDescription(product.getProductDescription());
        res.setPrice(product.getPrice());
        res.setUnit(product.getUnit());
        return res;
    }

    private ResponseDTO setResponse(CommonResStatus status, Object data) {
        ResponseDTO res = new ResponseDTO();
        res.setCode(status.getCode());
        res.setMessage(status.getMessage());
        res.setData(data);
        return res;
    }

    private boolean validateProduct(RequestCreateProductDTO req) {
        if (isNullOrEmpty(req.getProductName())) {
            return false;
        }
        if (isNullOrEmpty(req.getProductDescription())) {
            return false;
        }
        if (isNullOrEmpty(req.getPrice())) {
            return false;
        }
        if (isNullOrEmpty(req.getUnit())) {
            return false;
        }
        return true;
    }

    public static boolean isNullOrEmpty(String text) {
        return text == null || text.length() == 0;
    }

}
