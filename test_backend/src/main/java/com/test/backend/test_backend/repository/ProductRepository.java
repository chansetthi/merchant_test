package com.test.backend.test_backend.repository;

import com.test.backend.test_backend.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProductRepository extends JpaRepository<Product,Long> {
    Product findByUuid(String uuid);
    Page<Product> findAllByProductNameIgnoreCaseContaining(String name, Pageable pageable);
    boolean existsByProductName(String name);
    boolean existsByUuid(String uuid);
    Optional<Product> findById(Long id);
}
