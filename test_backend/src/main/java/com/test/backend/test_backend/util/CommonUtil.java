package com.test.backend.test_backend.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.charset.StandardCharsets;

public class CommonUtil {
    private static final Logger logger = LogManager.getLogger(CommonUtil.class);

    public static String parseObjectToJSON(Object object) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectWriter ow = mapper.writer().withDefaultPrettyPrinter();

        byte[] jsonBytes;
        try {
            jsonBytes = ow.writeValueAsBytes(object);
            return new String(jsonBytes, StandardCharsets.UTF_8);
        } catch (Exception e) {
            logger.error(e);
            e.printStackTrace();
        }

        return null;
    }

}
