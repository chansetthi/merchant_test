package com.test.backend.test_backend.service;

import com.test.backend.test_backend.dto.ResponseDTO;
import com.test.backend.test_backend.dto.merchant.RequestCreateMerchantDTO;
import com.test.backend.test_backend.dto.product.RequestAddProductDTO;
import com.test.backend.test_backend.dto.product.RequestCreateProductDTO;
import com.test.backend.test_backend.dto.product.RequestListProduct;

public interface ProductService {
    ResponseDTO createProduct(RequestCreateProductDTO req);
    ResponseDTO editProduct(RequestCreateProductDTO req);
    ResponseDTO deleteProduct(RequestCreateProductDTO req);
    ResponseDTO detailProduct(RequestCreateProductDTO req);
    ResponseDTO list(RequestListProduct req);
    ResponseDTO listInMerchant(RequestListProduct req);
    ResponseDTO addProduct(RequestAddProductDTO req);

}
