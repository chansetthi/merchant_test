package com.test.backend.test_backend.repository;

import com.test.backend.test_backend.entity.CategoryRefProduct;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRefProductRepository extends JpaRepository<CategoryRefProduct, Long> {

}
