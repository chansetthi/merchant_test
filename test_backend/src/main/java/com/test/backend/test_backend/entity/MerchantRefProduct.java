package com.test.backend.test_backend.entity;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "MERCHANT_REF_PRODUCT")
@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MerchantRefProduct extends BaseEntity{
    @ManyToOne
    Merchant merchant = new Merchant();
    @ManyToOne
    Product product = new Product();

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
}
