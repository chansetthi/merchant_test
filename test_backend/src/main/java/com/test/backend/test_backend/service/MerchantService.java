package com.test.backend.test_backend.service;

import com.test.backend.test_backend.dto.ResponseDTO;
import com.test.backend.test_backend.dto.merchant.RequestCreateMerchantDTO;
import com.test.backend.test_backend.dto.merchant.RequestListMerchant;

public interface MerchantService {
    ResponseDTO createMerchant(RequestCreateMerchantDTO req);
    ResponseDTO editMerchant(RequestCreateMerchantDTO req);
    ResponseDTO listMerchant(RequestListMerchant req);
    ResponseDTO detailMerchant(RequestCreateMerchantDTO req);
}
