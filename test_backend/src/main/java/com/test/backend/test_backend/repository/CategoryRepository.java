package com.test.backend.test_backend.repository;

import com.test.backend.test_backend.entity.Category;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {
    boolean existsByCategoryName(String name);
    boolean existsByUuidCategory(String uuid);
    Category findByUuidCategory(String uuid);
    Page findAllByCategoryNameIgnoreCaseContaining(String name , Pageable pageable);
}
