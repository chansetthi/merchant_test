package com.test.backend.test_backend.repository;


import com.test.backend.test_backend.entity.Merchant;
import com.test.backend.test_backend.entity.MerchantRefProduct;
import com.test.backend.test_backend.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MerchantRefProductRepository extends JpaRepository<MerchantRefProduct, Long> {
    Page<MerchantRefProduct> findByMerchant(Merchant merchant, Pageable pageable);
    boolean existsByMerchantAndProduct(Merchant merchant, Product product);
}
