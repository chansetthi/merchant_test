package com.test.backend.test_backend.implement;

import com.test.backend.test_backend.dto.PageDTO;
import com.test.backend.test_backend.dto.ResponseDTO;
import com.test.backend.test_backend.dto.merchant.RequestCreateMerchantDTO;
import com.test.backend.test_backend.dto.merchant.RequestListMerchant;
import com.test.backend.test_backend.dto.merchant.ResponseMerchantDetailDTO;
import com.test.backend.test_backend.dto.product.RequestAddProductDTO;
import com.test.backend.test_backend.entity.Merchant;
import com.test.backend.test_backend.enumuration.CommonResStatus;
import com.test.backend.test_backend.repository.MerchantRepository;
import com.test.backend.test_backend.service.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class MerchantServiceImpl implements MerchantService {
    @Autowired
    MerchantRepository merchantRepo;

    @Override
    public ResponseDTO createMerchant(RequestCreateMerchantDTO req) {
        UUID uuid = UUID.randomUUID();
        if (merchantRepo.existsByMerchantName(req.getMerchantName())) {
            return setResponse(CommonResStatus.DUPLICATE_MERCHANT_NAME, null);
        }
        Merchant merchant = new Merchant();
        merchant.setCreatedDate(new Date());
        if (!validateCreate(req)) {
            return setResponse(CommonResStatus.INVALID_INPUT_PARAMETER, null);
        }
        merchant.setMerchantName(req.getMerchantName());
        merchant.setMerchantDescription(req.getMerchantDescription());
        merchant.setPhoneNumber(req.getPhoneNumber());
        merchant.setAddress(req.getAddress());
        merchant.setUuid(uuid.toString());
        merchant.setVersion(1L);
        merchantRepo.save(merchant);

        return setResponse(CommonResStatus.SUCCESS, setResponseMerchant(merchant));
    }

    @Override
    public ResponseDTO editMerchant(RequestCreateMerchantDTO req) {
        Merchant merchant = merchantRepo.findByUuid(req.getUuid());
        if (merchantRepo.existsByMerchantName(req.getMerchantName())) {
            return setResponse(CommonResStatus.DUPLICATE_MERCHANT_NAME, null);
        }
        if (merchant == null){
            return setResponse(CommonResStatus.EXISTS_MERCHANT, null);
        }
        merchant.setLastUpdatedDate(new Date());
        merchant.setVersion(merchant.getVersion() + 1L);
        if (req.getMerchantName() != null) {
            merchant.setMerchantName(req.getMerchantName());
        }
        if (req.getMerchantDescription() != null) {
            merchant.setMerchantDescription(req.getMerchantDescription());
        }
        if (req.getPhoneNumber() != null) {
            merchant.setPhoneNumber(req.getPhoneNumber());
        }
        if (req.getAddress() != null) {
            merchant.setAddress(req.getAddress());
        }
        merchantRepo.save(merchant);
        return setResponse(CommonResStatus.SUCCESS, setResponseMerchant(merchant));
    }

    @Override
    public ResponseDTO listMerchant(RequestListMerchant req) {
        List<ResponseMerchantDetailDTO> res = new ArrayList<>();
        Pageable pageable = pageable(req.getPageSize(), req.getPageNumber());
        Page<Merchant> merchantList = merchantRepo.findAllByMerchantNameIgnoreCaseContaining(req.getText(), pageable);
        if (merchantList == null){
            return setResponse(CommonResStatus.SYSTEM_ERROR, null);
        }
        for (Merchant merchant : merchantList) {
            res.add(setResponseMerchant(merchant));
        }
        PageDTO pageDTO = new PageDTO();
        pageDTO.setContent(res);
        pageDTO.setPageNumber(merchantList.getNumber() + 1);
        pageDTO.setPageSize(merchantList.getSize());
        pageDTO.setTotalPages(merchantList.getTotalPages());
        pageDTO.setTotalElements(merchantList.getTotalElements());
        return setResponse(CommonResStatus.SUCCESS, pageDTO);
    }

    @Override
    public ResponseDTO detailMerchant(RequestCreateMerchantDTO req) {
        ResponseMerchantDetailDTO res = new ResponseMerchantDetailDTO();
        Merchant merchant = merchantRepo.findByUuid(req.getUuid());
        if (merchant == null) {
            return setResponse(CommonResStatus.EXISTS_MERCHANT, null);
        }
        return setResponse(CommonResStatus.SUCCESS, setResponseMerchant(merchant));
    }

    private ResponseMerchantDetailDTO setResponseMerchant(Merchant merchant) {
        ResponseMerchantDetailDTO res = new ResponseMerchantDetailDTO();
        res.setRemark(merchant.getRemark());
        res.setVersion(merchant.getVersion());
        res.setCreatedBy(merchant.getCreatedBy());
        res.setCreatedDate(merchant.getCreatedDate());
        res.setLastUpdatedBy(merchant.getLastUpdatedBy());
        res.setLastUpdatedDate(merchant.getLastUpdatedDate());

        res.setUuid(merchant.getUuid());
        res.setMerchantName(merchant.getMerchantName());
        res.setMerchantDescription(merchant.getMerchantDescription());
        res.setPhoneNumber(merchant.getPhoneNumber());
        res.setAddress(merchant.getAddress());
        return res;
    }

    private ResponseDTO setResponse(CommonResStatus status, Object data) {
        ResponseDTO res = new ResponseDTO();
        res.setCode(status.getCode());
        res.setMessage(status.getMessage());
        res.setData(data);
        return res;
    }

    public Pageable pageable(String pageSize, String pageNo) {
        int pageSizeInt = Integer.parseInt(pageSize);
        int pageNoInt = Integer.parseInt(pageNo);
        Pageable pageable = PageRequest.of(pageNoInt - 1, pageSizeInt);
        return pageable;
    }

    private Boolean validateCreate(RequestCreateMerchantDTO req) {
        if (isNullOrEmpty(req.getMerchantName())) {
            return false;
        }
        if (isNullOrEmpty(req.getMerchantDescription())) {
            return false;
        }
        if (isNullOrEmpty(req.getPhoneNumber())) {
            return false;
        }
        if (isNullOrEmpty(req.getAddress())) {
            return false;
        }
        return true;
    }

    public static boolean isNullOrEmpty(String text) {
        return text == null || text.length() == 0; // string.isEmpty() in Java 6
    }

}
