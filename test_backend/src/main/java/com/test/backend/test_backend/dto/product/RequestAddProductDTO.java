package com.test.backend.test_backend.dto.product;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class RequestAddProductDTO {
    private String uuidMerchant;
    private String uuidProduct;

    public String getUuidMerchant() {
        return uuidMerchant;
    }

    public void setUuidMerchant(String uuidMerchant) {
        this.uuidMerchant = uuidMerchant;
    }

    public String getUuidProduct() {
        return uuidProduct;
    }

    public void setUuidProduct(String uuidProduct) {
        this.uuidProduct = uuidProduct;
    }
}
