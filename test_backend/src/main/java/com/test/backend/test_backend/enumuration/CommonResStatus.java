package com.test.backend.test_backend.enumuration;

public enum  CommonResStatus {
    SUCCESS("0000","SUCCESS"),
    DUPLICATE_MERCHANT_NAME("0001","DUPLICATE MERCHANT NAME"),
    DUPLICATE_PRODUCT_NAME("0002","DUPLICATE PRODUCT NAME"),
    DUPLICATE_PRODUCT_IN_MERCHANT("0003","DUPLICATE PRODUCT IN MERCHANT"),
    DUPLICATE_CATEGORY("0004","DUPLICATE CATEGORY"),
    INVALID_INPUT_PARAMETER("0005","INVALID INPUT PARAMETER"),
    EXISTS_MERCHANT("0006","EXISTS MERCHANT"),
    EXISTS_PRODUCT("0007","EXISTS PRODUCT"),
    EXISTS_CATEGORY("0008","EXISTS CATEGORY"),
    SYSTEM_ERROR("9999","SYSTEM ERROR");

    private final String code;
    private final String message;
    CommonResStatus(final String code, final  String message){
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

}
