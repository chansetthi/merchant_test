package com.test.backend.test_backend.dto.product;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import com.test.backend.test_backend.dto.ResponseBaseData;

@JsonNaming(PropertyNamingStrategy.SnakeCaseStrategy.class)
public class ResponseProductDetailDTO extends ResponseBaseData {

    private String uuid;
    private String productName;
    private String productDescription;
    private String price;
    private String unit;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductDescription() {
        return productDescription;
    }

    public void setProductDescription(String productDescription) {
        this.productDescription = productDescription;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}
