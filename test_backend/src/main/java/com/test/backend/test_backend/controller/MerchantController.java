package com.test.backend.test_backend.controller;

import com.test.backend.test_backend.dto.merchant.RequestCreateMerchantDTO;
import com.test.backend.test_backend.dto.merchant.RequestListMerchant;
import com.test.backend.test_backend.util.CommonUtil;
import com.test.backend.test_backend.dto.ResponseDTO;
import com.test.backend.test_backend.service.MerchantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/merchant")
public class MerchantController extends CommonUtil {

    @Autowired
    MerchantService merchantService;

    @RequestMapping("/create")
    @ResponseBody
    public String createMerchant(@RequestBody RequestCreateMerchantDTO req){
        ResponseDTO res = merchantService.createMerchant(req);
        return parseObjectToJSON(res);
    }

    @RequestMapping("/edit")
    @ResponseBody
    public String editMerchant(@RequestBody RequestCreateMerchantDTO req){
        ResponseDTO res = merchantService.editMerchant(req);
        return parseObjectToJSON(res);
    }

    @RequestMapping("/list")
    @ResponseBody
    public String listMerchant(@RequestBody RequestListMerchant req){
        ResponseDTO res = merchantService.listMerchant(req);
        return parseObjectToJSON(res);
    }

    @RequestMapping("/detail")
    @ResponseBody
    public String detailMerchant(@RequestBody RequestCreateMerchantDTO req){
        ResponseDTO res = merchantService.detailMerchant(req);
        return parseObjectToJSON(res);
    }

}
